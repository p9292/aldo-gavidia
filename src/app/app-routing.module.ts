import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LandingComponent } from './layout/landing/landing.component';
import { AboutUsComponent } from './layout/about-us/about-us.component';
import { ServicesComponent } from './layout/services/services.component';
import { ContactComponent } from './layout/contact/contact.component';
import { CatalogueComponent } from './layout/catalogue/catalogue.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
  },
  {
    path: 'sobre-nosotros',
    component: AboutUsComponent,
  },
  {
    path: 'servicios',
    component: ServicesComponent,
  },
  {
    path: 'contacto',
    component: ContactComponent,
  },
  {
    path: 'catalogo',
    component: CatalogueComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
