import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavmenuComponent } from '../components/navmenu/navmenu.component';
import { FooterComponent } from '../components/footer/footer.component';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [NavmenuComponent, FooterComponent],
  imports: [CommonModule, RouterModule, TranslateModule],
  exports: [NavmenuComponent, FooterComponent],
})
export class CommonsModule {}
