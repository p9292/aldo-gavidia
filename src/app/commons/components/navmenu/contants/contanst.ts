export const constants: any = {
  menuList: [
    {
      name: 'Inicio',
      path: '/',
    },
    {
      name: 'Nosotros',
      path: '/sobre-nosotros',
    },
    {
      name: 'Servicios',
      path: '/servicios',
    },
    {
      name: 'Contacto',
      path: '/contacto',
    },
    {
      name: 'Catálogo',
      path: '/catalogo',
    },
  ],
};
