import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { constants } from './contants/contanst';

@Component({
  selector: 'app-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.scss'],
})
export class NavmenuComponent implements OnInit {
  languageSelected: string = 'en';
  isVisibleMenu: boolean = true;
  navMenuList: Array<any> = [];

  constructor(private translateService: TranslateService) {
    this.navMenuList = constants.menuList;
    this.translateService.setDefaultLang('es');
    this.translateService.use('en');
  }

  ngOnInit(): void {
    if (window.innerWidth >= 840) {
      this.isVisibleMenu = true;
    } else {
      this.isVisibleMenu = false;
    }

    window.addEventListener('resize', () => {
      if (window.innerWidth >= 840) {
        this.isVisibleMenu = true;
      } else {
        this.isVisibleMenu = false;
      }
    });
  }

  toggleMenu(): void {
    this.isVisibleMenu = !this.isVisibleMenu;
  }

  scrollTop(): void {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  selectLanguage(value: string) {
    this.languageSelected = value;
    this.translateService.use(value);
  }

  goToSection(id: string): void {
    const dom = document.getElementById(id);
    if (dom) {
      window.scrollTo({
        top: dom.offsetTop - 160,
        behavior: 'smooth',
      });
    }
  }
}
