import { animate, AnimationTriggerMetadata, style, transition, trigger } from "@angular/animations";

export const animations: Array<AnimationTriggerMetadata> = [
  trigger('enter', [
    transition(':enter', [
      style({
        opacity: 0,
        transform: 'translateY(-20px)'
      }),
      animate(500, style({
        opacity: 1,
        transform: 'translateY(0)'
      }))
    ]),
    transition(':leave', [
      animate(250, style({
        opacity: 0,
        transform: 'translateY(-20px)'
      }))
    ])
  ])
];
