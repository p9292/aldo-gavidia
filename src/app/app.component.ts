import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'landing-finance';

  constructor(
    public readonly translateService: TranslateService,
    private readonly meta: Meta
  ) {
    this.translateService.addLangs(['es', 'en', 'po']);

    this.meta.addTags([
      {
        name: 'title',
        content: 'DFD Corporation',
      },
      {
        name: 'language',
        content: 'Spanish',
      },
      {
        name: 'description',
        content:
          'Plataforma de excelencia financiera, innovadora y disruptiva que ayuda a cambiar y consolidar el capital que necesitas para la realización de tus proyectos.',
      },
      {
        name: 'keywords',
        content: 'finanzas, disruptivo, corporation, dfd, financiero, capital, tecnologia',
      },
      {
        name: 'author',
        content: 'DFD Corporation',
      },
      {
        name: 'robots',
        content: 'index,follow',
      },
    ]);
  }
}
