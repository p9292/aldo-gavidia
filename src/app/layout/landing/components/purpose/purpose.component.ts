import { Component, OnInit, Renderer2 } from '@angular/core';
import { InViewportMetadata } from 'ng-in-viewport';
import { constants } from './constants/contants';
@Component({
  selector: 'app-purpose',
  templateUrl: './purpose.component.html',
  styleUrls: ['./purpose.component.scss'],
})
export class PurposeComponent implements OnInit {
  purposeList: Array<any> = new Array();
  teamList = new Array();

  constructor(private readonly renderer: Renderer2) {}

  ngOnInit(): void {
    this.purposeList = constants.purposeList;
    this.teamList = [
      {
        name: 'Jaime Ramiro Lijeron',
        desc: `Presidente Ejecutivo`,
        image: 'jaime.png',
      },
      {
        name: 'Jorge Piñevsky',
        desc: `Vice Presidente Ejecutivo de Finanzas`,
        image: 'jorge_pinevsky.jfif',
      },
      {
        name: 'Hernán Arias',
        desc: `Presidente`,
        image: 'hernan.png',
      },
      {
        name: 'Aldo Gavidia',
        desc: `Director Ejecutivo`,
        image: 'aldo.png',
      },
      {
        name: 'Lorenzo Pessoa',
        desc: `Director Comercial`,
        image: 'lorenzop.jpeg',
      },
      {
        name: 'Alfred Michael Glenn Williams',
        desc: `Vicepresidente`,
        image: 'alfredm.jpeg',
      },
      {
        name: 'Ahmed Al Riyami',
        desc: `Director de Desarrollo de Negocios Internacionales`,
        image: 'ahmed.png',
      },
    ];
  }

  inViewPort(event: any) {
    const {
      [InViewportMetadata]: { entry },
      target,
      visible,
    } = event;

    const addClass = visible ? 'in-view' : 'out-view';
    this.renderer.addClass(target, addClass);

    const rmClass = visible ? 'out-view' : 'in-view';
    this.renderer.removeClass(target, rmClass);
  }
}
