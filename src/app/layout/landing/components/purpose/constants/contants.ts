export const constants = {
  purposeList: [
    {
      title: 'Investigación',
      description: `Expertos en distinguir el reto verdadero que presenta la organización, que nos permite crear soluciones exclusivas que potencien la generación de resultados financieros.`,
      path: '/',
      image: 'investigacion.jpeg',
    },
    {
      title: 'Desarrollo',
      description: `Entre la realidad deseada a la que aspira toda organización y la que tiene hoy como presente, la separa una delgada línea llamada desafío, por ello el diseño inteligente de conocimiento crea el puente que conduce llegar al resultado proyectado.`,
      path: '/',
      image: 'desarrollo-dis.jpeg',
    },
    {
      title: 'Implantación',
      description: `Otorgamos soluciones a tus necesidades de manera rápida y sencilla, a través de nuevas formulaciones para tu capitalización`,
      path: '/',
      image: 'implantacion.jpeg',
    },
    /* {
      title: 'Acercamiento',
      description: `Creemos que cuando hay colaboración, pueden suceder cosas extraordinarias, Las ideas encuentran formas de prosperar. Las empresas encuentran recursos para expandirse. Se abren oportunidades. Las relaciones se consolidan. El ecosistema se desarrolla.`,
      path: '/',
      image: 'acercamiento.jpg',
    }, */
    {
      title: 'Concentración tecnológica',
      description: `Ciencia de datos y algoritmos aplicados a la generación de métricas para la toma de decisiones, utilizando fuentes públicas o privadas, constantemente actualizadas o inclusive, conectadas en directo con el mercado como ocurre, por ejemplo, con las estrategias de trading automático con Inteligencia Artificial.`,
      path: '/',
      image: 'concentracion.jpeg',
    },
  ],
};
