import { Component, OnInit, Renderer2 } from '@angular/core';
import { InViewportMetadata } from 'ng-in-viewport';

@Component({
  selector: 'app-services-list',
  templateUrl: './services-list.component.html',
  styleUrls: ['./services-list.component.scss'],
})
export class ServicesListComponent implements OnInit {
  serviceList: Array<any> = new Array();

  constructor(private readonly renderer: Renderer2) {
    this.serviceList = [
      {
        title: '',
        icon: 'fa-usd',
        desc: 'Productos y Servicios Financieros Internacionales Disruptivos enmarcados dentro de los códigos de ética y moral internacional en el manejo de Fondos, Instrumentos Financieros Bancarios, Monedas y Cambio de Monedas y otros.',
      },
      {
        title: '',
        icon: 'fa-laptop',
        desc: 'Productos tecnológicos de alta capacidad de rendimiento en favor de las poblaciones.',
      },
      {
        title: '',
        icon: 'fa-bolt',
        desc: 'Productos de Comercio Internacional en general con un importante interés en aquellos productos dirigidos a la generación de energía y la salud.',
      },
    ];
  }

  ngOnInit(): void {}

  inViewPort(event: any) {
    const {
      [InViewportMetadata]: { entry },
      target,
      visible,
    } = event;

    const addClass = visible ? 'in-view' : 'out-view';
    this.renderer.addClass(target, addClass);

    const rmClass = visible ? 'out-view' : 'in-view';
    this.renderer.removeClass(target, rmClass);
  }
}
