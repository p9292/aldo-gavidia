import { Component, OnInit, Renderer2 } from '@angular/core';
import { InViewportMetadata } from 'ng-in-viewport';
import { Router } from '@angular/router';
@Component({
  selector: 'app-whoami',
  templateUrl: './whoami.component.html',
  styleUrls: ['./whoami.component.scss'],
})
export class WhoamiComponent implements OnInit {
  constructor(
    private readonly renderer: Renderer2,
    private readonly router: Router
  ) {}

  ngOnInit(): void {}

  inViewPort(event: any) {
    const {
      [InViewportMetadata]: { entry },
      target,
      visible,
    } = event;

    const addClass = visible ? 'in-view' : 'out-view';
    this.renderer.addClass(target, addClass);

    const rmClass = visible ? 'out-view' : 'in-view';
    this.renderer.removeClass(target, rmClass);
  }

  navigateAboutUsPage(): void {
    this.router.navigate(['/sobre-nosotros']);
  }
}
