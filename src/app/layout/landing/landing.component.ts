import { AfterViewInit, Component, OnInit } from '@angular/core';
import { animations } from '@shared/animations/animations';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  animations: [animations],
})
export class LandingComponent implements OnInit, AfterViewInit {
  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    const dom: any = document.getElementById('video-player');
    dom.muted = true;
    dom.autoplay = true;
    dom.playsinline = true;
  }
}
