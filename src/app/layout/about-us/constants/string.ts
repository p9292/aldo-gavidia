export const String = {
  values: [
    'Respeto',
    'Innovación',
    'Pensamiento disruptivo y global',
    'Usar y promover lo último en tecnología que sea más eficiente y eco-amigable',
    'Rapidez y excelencia',
    'Ética en todas nuestras actividades',
    'Pasión por mejorar el bienestar de las personas en el mundo',
    'Responsabilidad social y con el medio ambiente',
    'Enfoque en rentabilidad y costos',
  ],
};
