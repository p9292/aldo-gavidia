import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { animations } from '@shared/animations/animations';
import { String } from './constants/string';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
  animations: [animations],
})
export class AboutUsComponent implements OnInit {
  showMoreActive: boolean = false;
  listSectionInfo = new Array();

  langSelected: string = 'en';

  constants: any = String;

  constructor(private readonly translateService: TranslateService) {
    this.listSectionInfo = [
      {
        title: `Hambruna mundial`,
        description: `Eritrea, Burundi, Chad, Haití, Zambia, Yemen, Etiopía son solo algunos de los países donde el hambre mata a millones de personas cada año agravado recientemente, más aun por la guerra en Ucrania que privará al planeta de una parte muy importante de sus alimentos y de su abono`,
        image: 'pescado.jpeg',
      },
      {
        title: `Escasez de materias primas estratégicas`,
        description: `El grafito, el litio, el titanio, el níquel, el cobalto, el manganeso y los imanes son cada vez más escasos estos son vitales para las industrias tecnológicas.`,
        image: 'cabeza-oro.jpeg',
      },
      {
        title: `Crisis climática`,
        description: `Es una certeza que dentro de tres años, la humanidad habrá alcanzado un punto de no retorno y ya no podrá controlar la dinámica de la evolución de la temperatura del planeta.`,
        image: 'crisis-climatica.jpg',
      },
    ];
    this.showMoreActive = this.listSectionInfo.length > 2;

    this.translateService.onLangChange.subscribe((value: any) => {
      this.langSelected = value.lang;
    });
  }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }
}
