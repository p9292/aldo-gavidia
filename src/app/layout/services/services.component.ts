import { Component, OnInit } from '@angular/core';
import { animations } from '@shared/animations/animations';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
  animations: [animations],
})
export class ServicesComponent implements OnInit {
  serviceList: Array<any> = new Array();
  constructor() {
    this.serviceList = [
      {
        title: '',
        image: 'currency.jpg',
        desc: 'Productos y Servicios Financieros Internacionales Disruptivos enmarcados dentro de los códigos de ética y moral internacional en el manejo de Fondos, Instrumentos Financieros Bancarios, Monedas y Cambio de Monedas y otros.',
      },
      {
        title: '',
        image: 'tech.jpg',
        desc: 'Productos tecnológicos de alta capacidad de rendimiento en favor de las poblaciones.',
      },
      {
        title: '',
        image: 'energy.jpg',
        desc: 'Productos de Comercio Internacional en general con un importante interés en aquellos productos dirigidos a la generación de energía y la salud.',
      },
    ];
  }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }
}
