import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { InViewportModule } from 'ng-in-viewport';

import { CommonsModule } from './commons/modules/commons.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './layout/landing/landing.component';
import { AboutUsComponent } from './layout/about-us/about-us.component';
import { ServicesComponent } from './layout/services/services.component';
import { ContactComponent } from './layout/contact/contact.component';
import { CatalogueComponent } from './layout/catalogue/catalogue.component';
import { ServicesListComponent } from './layout/landing/components/services-list/services-list.component';
import { PurposeComponent } from './layout/landing/components/purpose/purpose.component';
import { WhoamiComponent } from './layout/landing/components/whoami/whoami.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    AboutUsComponent,
    ServicesComponent,
    ContactComponent,
    CatalogueComponent,
    ServicesListComponent,
    PurposeComponent,
    WhoamiComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    InViewportModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [HttpClient],
      },
    }),
    LoadingBarRouterModule,
    CommonsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
